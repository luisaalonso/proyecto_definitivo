import sys
import json
import os
import numpy as np
from simulacion import simulacion, lista_a_nparray 

try:
	ruta = sys.argv[1]
except IndexError:
	print "Error: debe especificar un parametro que sea una ruta!" 
#Intento abrir el archivo json en la ruta indicada
try:
	os.chdir(ruta)
	archivo = open('parametros.json')
except IOError:
	print 'La ruta espeficiada no contiene un archivo .json con nombre valido(parametros.json)!'
	exit()
except OSError:
	print 'El(Los) parametro(s) dado(s) no es(son) una ruta'
	exit()
#convierto los datos a un diccionario de python y cierro el archivo
try:
	parametros = json.load(archivo)
except ValueError:
	print 'El archivo .json no tiene el formato correcto'
	exit()
archivo.close()
#tomo en cuenta la posibilidad que el contenido del .json no sea el adecuado
try:
#meto los cuerpos dentro de una lista para reciclar el codigo utilizado en clase para el problema nbody
	cuerpos = parametros['cuerpos']
#Tomo los steps, el dt, intervalo, tolerancia
	steps = parametros['steps']
	dt = parametros['delta_t']
	intervalo = parametros['intervalo']
	tolerancia = parametros['tolerancia']
except KeyError:
	print 'Los datos en el archivo .json no son los necesarios para la simulacion'
	exit()
#Pregunto en que ruta desea guardar la simulacion, y considero todos los casos posibles al interactuar con usuario
print 'En que directorio desea guardar los avances de la simulacion?'
ruta = raw_input()
#Si el parametro dado no existe como ruta se pregunta si desea hacerla, si existe si revisa que tenga archivos o no
if os.path.isdir(ruta):
	os.chdir(ruta)
	lista_archivos = os.listdir(ruta)
	if lista_archivos:
#Si tiene archivos se pregunta que quiere hacer
		print 'La ruta contiene archivos desea: continuar o empezar otravez? (1 o 2)\nSe sobreescribiran los steps que se vuelvan a hacer.'
		proceso = raw_input()
		if proceso == '1':
#Se empieza la simulacion desde el step que se indique
			print 'Desde que step desea continuar?'
			steps = raw_input()
#Se intenta descargar los archivos del step dado
			try:
				datos = np.loadtxt(steps + '_step.csv', delimiter = ',')
#Se cargan las posiciones y velocidades al diccionario de cada cuerpo respectivo, y usar la funcion simulacion
				i = 0
				while i < len(cuerpos):
					cuerpos[i]['posicion'] = datos[i][0:3]
					cuerpos[i]['velocidad'] = datos[i][3:]
					i += 1
				steps = int(steps)
			except IOError:
				print 'El parametro especificado no existe como directorio, o no tiene un nombre valido(####_step.csv)!'
				exit()
		if proceso == '2':
#Se cargan los datos iniciales del json
#Se convierten a arreglos de numpy porque para los calculos se necesitan en este formato
			cuerpos = lista_a_nparray(cuerpos)
		if proceso != '1' and proceso != '2':
#Si no se especifico ninguna de las dos se termina el programa
			print 'No especifico que hacer. Se terminara la simulacion'
			exit()
	else:
#Si esta vacia se cargan los datos iniciales de .json, en formato de arreglo de numpy
		cuerpos = lista_a_nparray(cuerpos)
else:
#Si el parametro dado no es ruta se pregunta si se desea crearla
	print 'La ruta indicada no existe: desea crearla (s/n)?'
	proceso = raw_input()
	if proceso == 's':
#Si la desean crear, se crean y se mueve a esta carpeta para guardar los steps ahi
		os.mkdir(ruta)
		os.chdir(ruta)
#Se cargan condiciones iniciales en formate de arreglo numpy
		cuerpos = lista_a_nparray(cuerpos)
	#Sino la desean crear se termina el programa
	else:
		exit()
#finalmente se llama a la funcion de simulacion que guardara los progresos de la simulacion en la ruta especificada
simulacion(cuerpos, dt, steps, intervalo, tolerancia)
