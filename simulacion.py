from integrador import euler_step
import numpy as np
from calculos import calcula_energia_total
#modulo para simplificar el codigo de nbody

#funcion hecha para reciclar el codigo de convertir las listas de posicion y velocidades a lista de numpy
def lista_a_nparray(cuerpos):
	for cuerpo in cuerpos:
		cuerpo['posicion'] = np.array(cuerpo['posicion'])
		cuerpo['velocidad'] = np.array(cuerpo['velocidad'])
	return cuerpos

#basado en el codigo visto en clase para el problema de ncuerpos
def simulacion(cuerpos, dt, steps, intervalo, tolerancia):
# Guardamos la energia total al inicio de la simulacion
# para verificar que al final el sistema conserve la energia
	etot_inicial = calcula_energia_total(cuerpos)

	while steps >= 0:
		euler_step(cuerpos, dt)
    # Mensaje para ir viendo el avance del proceso
		if steps % 1000 == 0:
			print "Faltan %d steps " % (steps)
#Busco acomodar los datos como matrices, que cada fila contenga la posiciones y velocidad de cada cuerpo, para guardar los datos asi en un .csv
#Utilizo la funcion append de numpy para unir las velocidad y posiciones de un cuerpo en una fila y meterlo como listas en el vector vacio para mi matriz
		if steps % intervalo == 0:
			i = len(cuerpos)
			posiciones_y_velocidades = []
			while i > 0:
				posiciones_y_velocidades.append(np.append(cuerpos[len(cuerpos)-i]['posicion'],cuerpos[len(cuerpos)-i]['velocidad']))
				i -= 1
#Guardo con la funcion savetxt de numpy, con la intencion de que quede en el mismo formato de los archivos usados en la tarea#2 
			np.savetxt(str(steps) + '_step.csv', posiciones_y_velocidades, delimiter = ',', header = 'x,y,z,vx,vy,vz')
		steps -= 1
		etot_final = calcula_energia_total(cuerpos)
#calculo la diferencia entre energia final e inicial para ver si cumplen la tolerancia
		diff = abs((etot_final - etot_inicial)/etot_inicial)
		if diff > tolerancia:
			print 'El sistema sobrepaso la tolerancia con {porcentaje}%'.format(porcentaje = diff*100)
			exit()
	print "Energia total inicial: %s" % (str(etot_inicial))
	print "Energia total final: %s" % (str(etot_final))
