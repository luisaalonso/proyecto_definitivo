import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import animation
from utils import cargar_datos, obtener_minimos, obtener_maximos
#Tomando la solucion aportado por el profesor de la Tarea#2

ruta = raw_input("Digite la ruta que contiene los archivos: ")

try:
    archivos = os.listdir(ruta)
except OSError:
    print "Ruta invalida"
    exit(1)
#Esta la unica modificacion, se preguntar los parametros iniciales y finales
inicio = int(raw_input('Donde quiere empezar?'))
fin = int(raw_input('Donde quiere terminar?'))

steps, contenido = cargar_datos(ruta, archivos, inicio, fin)

print "Archivos cargados en memoria"

# -----

fig = plt.figure("Animacion")

ax = plt.axes(projection='3d')

sp, = ax.plot([], [], [], 'r.')

minimos = obtener_minimos(contenido)
maximos = obtener_maximos(contenido)

print "Minimos y maximos: ", minimos, maximos

ax.set_xlim(minimos[0], maximos[0])
ax.set_ylim(minimos[1], maximos[1])
ax.set_zlim(minimos[2], maximos[2])

def update(i):
    indice = steps[i]
    # Saco del diccionario los datos para este archivo (frame)
    valores = contenido[indice]
    x = valores[0]
    y = valores[1]
    z = valores[2]
    sp.set_data(x, y)
    sp.set_3d_properties(z) 
    #ax.set_title('Step %s' % indice)
    return sp,

ani = animation.FuncAnimation(fig, update, frames=len(contenido.keys()), interval=50, repeat=True)
plt.show()

